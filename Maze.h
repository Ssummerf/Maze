#ifndef MAZE
#define MAZE
#include <string>
#include <stack>
#include <queue>
#include <cctype>

#include "Space.h"

class Maze {
public:
     Maze();
     Maze(string maze, int initialR, int initialC);
     bool setInitial(int r, int c);
     Space getSpace(int r, int c);
     stack<Space> getFreeNeighborsStack(Space currentSpace);

     bool findExitStack(Space currentSpace);
     stack<Space> getSolutionStack();
     queue<Space>  getFreeNeighborsQueue(Space currentSpace);
     bool findExitQueue(Space currentSpace);
     queue<Space>  getSolutionQueue();

     void errorFound(int n);
     int getRows();
     int getCols();
     void visit(int row, int col);
     void Xvisit(int row, int col);
     void setStart();

private:

     //All the Spaces in a two dimensional array
     Space grid[15][15];
     //The total number of rows and colums
     int rows, cols;
     //The current row and column of the Space
     int currentR, currentC;
     //Stack of the found free neighbors in the stack
     stack<Space> freeNeighborsStack;

     //Stack of the solution Spaces, to be printed to the file
     stack<Space>  solutionStack;
     //Stack of the found free neighbors in the queue
     queue<Space> freeNeighborsQueue;
     //Queue of the solution Spaces, to be printed to the file
     queue<Space> solutionQueue;
};

#include "Maze.cpp"
#endif

 