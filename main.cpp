#include <iostream>
#include <string>
#include <cstdlib>
#include "Space.h"
#include "Maze.h"


using namespace std;

//Print out the space type
void printStuff(Maze Dungeon, int checker){

  for(int i = 0; i < Dungeon.getRows(); i++){
      for(int j = 0; j < Dungeon.getCols(); j++){

        Space temp = Dungeon.getSpace(i,j);
        if(temp.getType() == ' ')
          cout << " ";
        if(temp.getType() == '*')
          cout << "*";
        if(temp.getType() == 'V')
          cout << "P";
        if(temp.getType() == 'X' && checker == 0)
          cout << "P";
        if(temp.getType() == 'X' && checker == 1)
          cout << " ";
      }
      cout << endl;
    }
}

//Run through using a stack
void runStack(Maze Dungeon){

  Space runner = Dungeon.getSpace(1,0);
  Space neighbor;
  stack<Space> endme;
  endme.push(runner);


  //Initial print
  printStuff(Dungeon,0);
  cout << endl << endl;
  //Stack Checking
  Dungeon.setStart();
  while(!endme.empty()){

    //Mark as visited and pop
    runner = endme.top();
    Dungeon.visit(runner.getRow(), runner.getCol());
    endme.pop();

    stack<Space> lonesome = Dungeon.getFreeNeighborsStack(runner);

    //If it has neighbors, take a random unvisited neighbor and add it to the stack, 
    //push the current spot and the neighbor selected to the stack
    if(!lonesome.empty()){
    int randy = rand() % lonesome.size();

    for(int wowzer = 2; wowzer < randy; wowzer++){
      lonesome.pop();
    }

    endme.push(runner);
    endme.push(lonesome.top());


    //Pushing both of our neighbor holding cells
    

    //Print statement
    printStuff(Dungeon,0);
    cout << endl << endl;
    }else if(runner.getRow() == 0 || runner.getRow() == Dungeon.getRows()-1){
      Dungeon.Xvisit(runner.getRow(), runner.getCol());
    }
  }

  printStuff(Dungeon,0);
  cout << endl;
  cout << "Stack Solution" << endl;
  printStuff(Dungeon,1);
  cout << endl;
}

//Run through using a queue
void runQueue(Maze Dungeon){
  Space runner = Dungeon.getSpace(1,0);
  Space neighbor;
  queue<Space> endme;
  endme.push(runner);
  int ignoreOne = 1;

  while(!endme.empty()){

    //Mark as visited and pop
    Dungeon.visit(runner.getRow(), runner.getCol());
    endme.pop();
    
    if(Dungeon.findExitQueue(runner))
      break;

    queue<Space> lonesome = Dungeon.getFreeNeighborsQueue(runner);

    while(!lonesome.empty()){
      endme.push(lonesome.front());
      lonesome.pop();
    }

    runner = endme.front();

    //Print statement
      cout << endl;
      printStuff(Dungeon,0);
      cout << endl;
    

    ignoreOne++;
    }

    cout << "Queue Solution" << endl;
    printStuff(Dungeon,0);
}
//Main Program - Asks for file, then runs the Stack / Queue solutions
int main ( int argc, char *argv[] ){

  //Pulling our filename arguement 
  string lines = "";
  if ( argc != 2 )
    cout<<"usage: "<< argv[0] <<" <filename>\n";
  

    string name = argv[1];
    //Running dungeon
    Maze Dungeon(name, 1, 0);
    runStack(Dungeon);

    Dungeon = Maze(name, 1, 0);
    runQueue(Dungeon);
  

  return 0;

}