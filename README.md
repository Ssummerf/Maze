# C++ Maze Simulator

Given a Maze in text format, this program will solve it by using a stack and a queue. The code is written in C++, and incorporates object oriented programming, abstraction, and recursion.

In short, a pain in the ass. 

As always, additional information regarding the methodology can be found in the comments of each file.

## Space

Our object placed in each square of the grid. It contains its type (Wall, Empty, Etc.) and setters / getters

## Maze

Given a filename, this will fill up our Maze with Space objects, and provide methods for solving our Maze with a stack or a queue.

## Main

The main runner of our program. This displays our Maze on each iteration, and contains the recursive methods used to solve our stack/queue. 

## Txt Files

The initial configuration for our Mazes. They need to be in specific formats - only numbers and single spaced. Walls are defined, empty spaces are defaulted.

