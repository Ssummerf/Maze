#ifndef MAZECPP
#define MAZECPP

#include <iostream>
#include <string>
#include <vector>
#include <iomanip>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <cctype>

#include "Maze.h"

using namespace std;

Maze::Maze(){
     Maze("Maze.txt", 1, 0);
}

Maze::Maze(string maze, int initialR, int initialC){

int counter = 0;
string delim = " ";

size_t prev = 0, pos = 0;
int placer, placer2;

//Reading Files
string lines = "";
string lineRead = "";
ifstream filereader;
filereader.open(maze);
//Bad name case
if (!filereader.is_open()){
          cout << "Could not open file!" << endl;
}   

while(getline(filereader, lineRead)){
     lines += lineRead;
}
filereader.close();

string maze2 = lines;
//Start by filling the grid with free Spaces
//We initially declare our array as a 15 x 15 due to C++ constraints, but only access the portion that's within our boundaries.
for(int i = 0; i < 15; i++){
     for(int j = 0; j < 15; j++){
               Space temper(' ',i,j);
               this->grid[i][j] = temper;
     }
}

//Rip out the new lines
string str = maze2;
str.erase(std::remove(str.begin(), str.end(), '\n'), str.end());

//Rip out the Spaces
std::string::iterator end_pos = std::remove(str.begin(), str.end(), ' ');
str.erase(end_pos, str.end());
//Loop through the maze, set up the size and the wall positions
for(int i = 0; i < str.length(); i+=2){
     placer = str[i] - 48;
     placer2 = str[i+1] - 48;

     if(counter == 0){
          this->rows = placer;
          this->cols = placer2;
     }else{
          Space temp('*', placer2, placer);
          this->grid[placer][placer2] = temp;
     }

     counter++;
}

//Iniital positions
setInitial(initialR, initialC);
setInitial(1,0);


}

//Set the initial positions if they're within bounds
bool Maze::setInitial(int r, int c){
     if(r < rows && c < cols && c >= 0 && r >= 0 ){
     this-> currentR = r;
     this-> currentC = c;
     return true;
     }else
          return false;
}

//Returns a Space position
Space Maze::getSpace(int r, int c){
     return grid[r][c];
}

/**
* Given a specific Space element, it returns a set of Spaces around it that are free.
*
* @param currentSpace The Space of which the neighbors need to be checked
* @return Returns the set of free Spaces neighboring in a stack structure
* */
stack<Space> Maze::getFreeNeighborsStack(Space currentSpace){
     stack<Space> returner;
     int cr = currentSpace.getRow();
     int cc = currentSpace.getCol();

     //Check the four adjacent squares, if they're within boundaries, and push them to the stack
      if(cr < rows && cc+1 < cols && cc+1 >= 0 && cr >= 0)
          if(getSpace(cr,cc+1).getType() == ' ')
               returner.push(getSpace(cr,cc+1));

     if(cr < rows && cc-1 < cols && cc-1 >= 0 && cr >= 0)
          if(getSpace(cr,cc-1).getType() == ' ')
               returner.push(getSpace(cr,cc-1));

     if(cr+1 < rows && cc < cols && cc >= 0 && cr+1 >= 0)
          if(getSpace(cr+1,cc).getType() == ' ')
               returner.push(getSpace(cr+1,cc));

     if(cr-1 < rows && cc < cols && cc >= 0 && cr-1 >= 0)
          if(getSpace(cr-1,cc).getType() == ' ')
               returner.push(getSpace(cr-1,cc));

    


     return returner;
}
/**
* Finds the exit from a given Space, if found, returns true otherwise it returns false
*
* @param currentSpace The Space that is being focused on, the one from which it finds the exit
* @return True if an exit is found, false if not (the stack is empty)
* */
bool Maze::findExitStack(Space currentSpace){
     if(currentSpace.getType() == ' ' && currentSpace.getCol() == cols)
          return true;
     else
          return false;
}
/**
* Returns the solution stack with all the Spaces that have been visited
*
* @return The stack containing the solution
* */
stack<Space> Maze::getSolutionStack(){
     stack<Space> returner;

     for(int i = 0; i < rows; i++){
          for(int j = 0; j < cols; j++){
               if(getSpace(i,j).getType() == 'V')
                    returner.push(getSpace(i,j));
     }
}
     return returner;
}
/**
* Given a specific Space element, it returns a set of Spaces around it that are free.
*
* @param currentSpace The Space of which the neighbors need to be checked
* @return Returns the set of free Spaces neighboring in a queue structure
* */
queue<Space> Maze::getFreeNeighborsQueue(Space currentSpace){
     queue<Space> returner;

     int cr = currentSpace.getRow();
     int cc = currentSpace.getCol();

     //Check the four adjacent squares, if they're within boundaries, and push them to the stack
     if(cr-1 < rows && cc < cols && cc >= 0 && cr-1 >= 0)
          if(getSpace(cr-1,cc).getType() == ' ')
               returner.push(getSpace(cr-1,cc));
     if(cr+1 < rows && cc < cols && cc >= 0 && cr+1 >= 0)
          if(getSpace(cr+1,cc).getType() == ' ')
               returner.push(getSpace(cr+1,cc));

     if(cr < rows && cc-1 < cols && cc-1 >= 0 && cr >= 0)
          if(getSpace(cr,cc-1).getType() == ' ')
               returner.push(getSpace(cr,cc-1));
     if(cr < rows && cc+1 < cols && cc+1 >= 0 && cr >= 0)
          if(getSpace(cr,cc+1).getType() == ' ')
               returner.push(getSpace(cr,cc+1));

     return returner;
}
/**
* Finds the exit from a given Space, if found, returns true otherwise it returns false
*
* @param currentSpace The Space that is being focused on, the one from which it finds the exit
* @return True if an exit is found, false if not (the queue is empty)
* */
bool Maze::findExitQueue(Space currentSpace){
     if(currentSpace.getType() == ' ' && currentSpace.getCol() == cols)
          return true;
     else
          return false;
}
/**
* Gets the solution in the queue format
*
* @return A set of Spaces in the format of the queue
* */
queue<Space>  Maze::getSolutionQueue(){
     queue<Space> returner;

     for(int i = 0; i < rows; i++){
          for(int j = 0; j < cols; j++){
               if(getSpace(i,j).getType() == 'V')
                    returner.push(getSpace(i,j));
     }
     }
     return returner;
}
//Method that manages errors, depending on a certain index
void Maze::errorFound(int n){
     if(n < 0)
          cout << "Out of bounds error on index n!" << endl;
}

int Maze::getRows(){
     return this->rows;
}

int Maze::getCols(){
     return this->cols;
}

void Maze::visit(int row, int col){
     Space temp;
     temp = getSpace(row,col);
     temp.setType('V');
     grid[row][col] = temp;
}

void Maze::Xvisit(int row, int col){
     Space temp;
     temp = getSpace(row,col);
     temp.setType('X');
     grid[row][col] = temp;
}

void Maze:: setStart(){
     Space temp;
     temp = getSpace(1,0);
     temp.setType(' ');
     grid[1][0] = temp;
}
#endif