#ifndef SpaceCPP
#define SpaceCPP

#include "Space.h"
#include <string>

using namespace std;
Space::Space(){
	this->type.resize(2);
	this->row = 0;
	this->col = 0;
	this->type[0] = 'X';
}
Space::Space(char type, int row, int col){
	this->type.resize(2);
	this->row = row;
	this->col = col;
	this->type[0] = type;
}
int Space::getRow(){
	return row;
}

char Space::getType(){
	return type[0];
}
void Space::setType(char type){
	this->type[0] = type;
}

int Space::getCol(){
	return col;
}

#endif