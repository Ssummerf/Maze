#ifndef SPACE
#define SPACE
#include <vector>
#include <string>

using namespace std;
class Space {
public:

     Space();
     Space(char type, int row, int col);
     int getRow();
     char getType();
     void setType(char type);
     int getCol();

private:

     vector<char> type;
     int row;
     int col;

};

#include "Space.cpp"
#endif